package es.amazon.palindrome.helpers;

import junit.framework.Assert;

import org.junit.Test;

public class PalindromeHelperTest {

	private static final String TEST_PAL = "8888";
	private static final String TEST_MASTER_STRING = "445551238888000000";

	@Test
	public void testLongestPalindrome() {
		String pal = PalindromeHelper.longestPalindrome(TEST_MASTER_STRING, 4);
		Assert.assertTrue(pal.equals(TEST_PAL));
	}

}
