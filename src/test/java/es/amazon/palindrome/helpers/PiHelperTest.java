package es.amazon.palindrome.helpers;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;

public class PiHelperTest {
	
	@Test
	public void testPi() {
		BigDecimal pi = PiHelper.pi(1000);
		Assert.assertTrue(pi != null);
		String piStr = pi.toString();
		Assert.assertTrue(piStr.length() >= 1000);
	}

}
