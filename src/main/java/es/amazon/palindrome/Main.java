package es.amazon.palindrome;

import java.io.IOException;

import es.amazon.palindrome.helpers.PalindromeHelper;
import es.amazon.palindrome.helpers.PiHelper;

public class Main {
	
	public static void main(String[] args) throws IOException {
		System.out.println(PalindromeHelper.longestPalindrome(PiHelper.pi(20000).toString(), 10));
	}
}
